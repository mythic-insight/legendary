import Config

import_config "email_styles.exs"
import_config "admin.exs"

host =
  case config_env() do
    env when env in [:dev, :e2e, :test] -> "localhost"
    _ -> System.fetch_env!("HOSTNAME")
  end

[
  {:admin, Legendary.Admin.Repo},
  {:app, App.Repo},
  {:content, Legendary.Content.Repo},
  {:core, Legendary.Core.Repo},
  {:object_storage, Legendary.ObjectStorage.Repo}
]
|> Enum.map(fn {otp_app, repo} ->
  config otp_app,
    ecto_repos: [repo],
    generators: [context_app: otp_app]

  config otp_app, :host, host
end)

# Configures Elixir's Logger
config :logger, :console,
  level: :debug,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :core, :pow,
  user: Legendary.Auth.User,
  repo: Legendary.Core.Repo,
  extensions: [PowEmailConfirmation, PowPersistentSession, PowResetPassword],
  controller_callbacks: Legendary.AuthWeb.Pow.ControllerCallbacks,
  mailer_backend: Legendary.AuthWeb.Pow.Mailer,
  web_mailer_module: Legendary.AuthWeb,
  web_module: Legendary.AuthWeb,
  cache_store_backend: Pow.Store.Backend.MnesiaCache

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :linguist, pluralization_key: :count

# Set default values to the Post or Page admin form fields
config :content,
  post_admin: [
    default: [
      # Publish or Draft
      status: "Publish",
      # open or closed
      comment_status: "open",
      # open or closed
      ping_status: "open"
    ]
  ]

config :app,
      Oban,
      repo: App.Repo,
      queues: [default: 10],
      crontab: [
        {"0 * * * *", Legendary.Content.Sitemaps}
      ]

# Feature flags

config :fun_with_flags, :cache,
  enabled: true,
  # seconds
  ttl: 300

config :fun_with_flags, :persistence,
  adapter: FunWithFlags.Store.Persistent.Ecto,
  repo: Legendary.Core.Repo

config :fun_with_flags, :cache_bust_notifications,
  enabled: true,
  adapter: FunWithFlags.Notifications.PhoenixPubSub,
  client: App.PubSub

# Disable cache bust notifications for fun_with_flags so that we don't need redis
config :fun_with_flags, :cache_bust_notifications, enabled: false

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.15.8",
  node: [
    "build.mjs --watch",
    cd: Path.expand("../apps/app/assets/", __DIR__),
    env: %{"NODE_ENV" => "development"}
  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

config :object_storage,
  bucket_name: "uploads"
