import Config

config :core, Legendary.CoreMailer, adapter: Bamboo.TestAdapter

config :app, Oban, crontab: false, queues: false, plugins: false

config :logger, level: :warn

config :waffle,
  storage: Waffle.Storage.Local,
  storage_dir_prefix: "priv/test/static/"

config :object_storage,
       :signature_generator,
       Legendary.ObjectStorageWeb.CheckSignatures.MockSignatureGenerator
