import Config

defmodule App.RuntimeConfig.Dev do
  def secret_key_base,
    do:
      System.get_env(
        "SECRET_KEY_BASE",
        "r2eN53mJ9RmlGz9ZQ7xf43P3Or59aaO9rdf5D3hRcsuiH44pGW9kPGfl5mt9N1Gi"
      )

  def live_view_signing_salt, do: System.get_env("LIVE_VIEW_SIGNING_SALT", "g5ltUbnQ")
  def default_url_port, do: "4000"
  def default_listen_port, do: "4000"

  def endpoint_extra_opts(:app) do
    [
      live_reload: [
        patterns: [
          ~r"priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$",
          ~r"priv/gettext/.*(po)$",
          ~r"(live|views)/.*(ex)$",
          ~r"templates/.*(eex)$"
        ]
      ]
    ] ++
      default_endpoint_extra_opts(:app)
  end

  def endpoint_extra_opts(otp_app), do: default_endpoint_extra_opts(otp_app)

  def default_endpoint_extra_opts(_otp_app) do
    if System.get_env("RELEASE_NAME") do
      []
    else
      [
        debug_errors: true,
        code_reloader: true,
        check_origin: false,
        watchers: [
          node: [
            "build.mjs",
            "--watch",
            cd: Path.expand("../apps/app/assets/", __DIR__),
            env: %{"NODE_ENV" => "development"}
          ]
        ]
      ]
    end
  end

  def ecto_pool(), do: Legendary.Core.SharedDBConnectionPool

  def db_config() do
    [
      username: "postgres",
      password: "postgres",
      database: "legendary_dev",
      hostname: "localhost",
      show_sensitive_data_on_connection_error: true
    ]
  end

  def mailer_config(), do: []

  def cluster_topologies() do
    [
      erlang_hosts: [
        strategy: Elixir.Cluster.Strategy.Gossip
      ]
    ]
  end

  def object_storage_scheme(), do: "http://"
  def object_storage_host(), do: "localhost"
  def object_storage_port(), do: 4000
  def object_storage_bucket(), do: "uploads"
  def object_storage_access_key_id(), do: "dev-test-access-key-id"
  def object_storage_secret_access_key(), do: "dev-test-secret-access-key"
end

defmodule App.RuntimeConfig.Test do
  def secret_key_base,
    do:
      System.get_env(
        "SECRET_KEY_BASE",
        "r2eN53mJ9RmlGz9ZQ7xf43P3Or59aaO9rdf5D3hRcsuiH44pGW9kPGfl5mt9N1Gi"
      )

  def live_view_signing_salt, do: System.get_env("LIVE_VIEW_SIGNING_SALT", "g5ltUbnQ")
  def url_host, do: "localhost"
  def default_url_port, do: "4000"
  def default_listen_port, do: "4000"
  def endpoint_extra_opts(_), do: []

  def ecto_pool(), do: Ecto.Adapters.SQL.Sandbox

  def db_config() do
    [
      username: "postgres",
      password: "postgres",
      database: "legendary_test#{System.get_env("MIX_TEST_PARTITION")}",
      hostname: System.get_env("DATABASE_URL") || "localhost"
    ]
  end

  def mailer_config(), do: []

  def cluster_topologies(), do: []

  def object_storage_scheme(), do: "http://"
  def object_storage_host(), do: "localhost"
  def object_storage_port(), do: App.RuntimeConfig.url_port()
  def object_storage_bucket(), do: "uploads"
  def object_storage_access_key_id(), do: "test-access-key-id"
  def object_storage_secret_access_key(), do: "test-secret-access-key"
end

defmodule App.RuntimeConfig.E2E do
  def secret_key_base,
    do:
      System.get_env(
        "SECRET_KEY_BASE",
        "r2eN53mJ9RmlGz9ZQ7xf43P3Or59aaO9rdf5D3hRcsuiH44pGW9kPGfl5mt9N1Gi"
      )

  def live_view_signing_salt, do: System.get_env("LIVE_VIEW_SIGNING_SALT", "g5ltUbnQ")
  def default_url_port, do: "4002"
  def default_listen_port, do: "4002"
  def endpoint_extra_opts(_), do: []

  def ecto_pool(), do: Ecto.Adapters.SQL.Sandbox

  def db_config() do
    [
      username: "postgres",
      password: "postgres",
      database: "legendary_test#{System.get_env("MIX_TEST_PARTITION")}",
      hostname: System.get_env("DATABASE_URL") || "localhost"
    ]
  end

  def mailer_config(), do: []

  def cluster_topologies(), do: []

  def object_storage_scheme(), do: "http://"
  def object_storage_host(), do: "localhost"
  def object_storage_port(), do: 4000
  def object_storage_bucket(), do: "uploads"
  def object_storage_access_key_id(), do: "test-access-key-id"
  def object_storage_secret_access_key(), do: "test-secret-access-key"
end

defmodule App.RuntimeConfig.Prod do
  def secret_key_base, do: System.fetch_env!("SECRET_KEY_BASE")
  def live_view_signing_salt, do: System.fetch_env!("LIVE_VIEW_SIGNING_SALT")
  def default_url_port, do: "80"
  def default_listen_port, do: "4000"

  def endpoint_extra_opts(:app),
    do: [
      cache_static_manifest: "priv/static/cache_manifest.json",
      check_origin: origins(:app)
    ]

  def endpoint_extra_opts(app), do: [check_origin: origins(app)]

  defp origins(_app) do
    hostname = System.get_env("HOSTNAME")
    alternate_hostnames = String.split(System.get_env("ALTERNATE_HOSTNAMES", ""), ",")

    fly_host =
      case System.get_env("FLY_APP_NAME") do
        name when is_binary(name) ->
          ["https://#{name}.fly.dev"]

        _ ->
          []
      end

    [hostname] ++ alternate_hostnames ++ fly_host
  end

  def ecto_pool(), do: Legendary.Core.SharedDBConnectionPool
  def db_config(), do: [url: System.fetch_env!("DATABASE_URL")]

  def mailer_config() do
    [
      server: {:system, "SMTP_HOST"},
      hostname: {:system, "HOSTNAME"},
      port: 25,
      username: {:system, "SMTP_USERNAME"},
      password: {:system, "SMTP_PASSWORD"},
      tls: :if_available,
      allowed_tls_versions: [:tlsv1, :"tlsv1.1", :"tlsv1.2"],
      ssl: false,
      retries: 1,
      no_mx_lookups: false,
      auth: :always
    ]
  end

  def cluster_topologies() do
    case System.get_env("FLY_APP_NAME") do
      fly_app_name when is_binary(fly_app_name) ->
        cluster_topologies_for_provider(:fly, fly_app_name)

      _ ->
        cluster_topologies_for_provider(:k8)
    end
  end

  defp cluster_topologies_for_provider(:fly, app_name) do
    [
      fly6pn: [
        strategy: Cluster.Strategy.DNSPoll,
        config: [
          polling_interval: 5_000,
          query: "#{app_name}.internal",
          node_basename: System.get_env("NAME", "legendary")
        ]
      ]
    ]
  end

  defp cluster_topologies_for_provider(:k8) do
    [
      kubernetes: [
        strategy: Elixir.Cluster.Strategy.Kubernetes,
        config: [
          mode: :ip,
          kubernetes_ip_lookup_mode: :pods,
          kubernetes_node_basename: System.get_env("NAME", "legendary"),
          kubernetes_selector: "app=#{System.get_env("NAME", "legendary")}",
          kubernetes_namespace: System.get_env("NAMESPACE", "legendary"),
          polling_interval: 10_000
        ]
      ]
    ]
  end

  def object_storage_scheme(), do: "https://"
  def object_storage_host(), do: System.fetch_env!("HOSTNAME")
  def object_storage_port(), do: App.RuntimeConfig.url_port()
  def object_storage_bucket(), do: "uploads"
  def object_storage_access_key_id(), do: System.fetch_env!("OBJECT_STORAGE_ACCESS_KEY_ID")

  def object_storage_secret_access_key(),
    do: System.fetch_env!("OBJECT_STORAGE_SECRET_ACCESS_KEY")
end

defmodule App.RuntimeConfig do
  def config, do: config(config_env())
  def config(:dev), do: App.RuntimeConfig.Dev
  def config(:test), do: App.RuntimeConfig.Test
  def config(:e2e), do: App.RuntimeConfig.E2E
  def config(:prod), do: App.RuntimeConfig.Prod

  def app_name(), do: System.get_env("NAME", "legendary")
  def app_namespace(), do: System.get_env("NAMESPACE", "legendary")

  def secret_key_base, do: config().secret_key_base()
  def live_view_signing_salt, do: config().live_view_signing_salt()
  def session_signing_salt, do: System.get_env("SESSION_SIGNING_SALT", "VQMRz57g")

  def server_command?() do
    cond do
      System.get_env("RELEASE_NAME") ->
        !!System.get_env("PHX_SERVER")

      config_env() == :e2e ->
        true

      true ->
        Application.get_env(:phoenix, :serve_endpoints, false)
    end
  end

  def url_host(), do: Application.get_env(:app, :host)

  def url_port(),
    do: "URL_PORT" |> System.get_env(config().default_url_port()) |> String.to_integer()

  def listen_port(),
    do: "PORT" |> System.get_env(config().default_listen_port()) |> String.to_integer()

  def endpoint_extra_opts(otp_app), do: config().endpoint_extra_opts(otp_app)

  def ecto_pool(), do: config().ecto_pool()
  def db_config(), do: config().db_config()

  def ecto_use_ipv6() do
    if System.get_env("ECTO_IPV6"), do: [:inet6], else: []
  end

  def pool_size(), do: String.to_integer(System.get_env("POOL_SIZE", "10"))

  def email_from(), do: System.get_env("EMAIL_FROM", "example@#{url_host()}")
  def mailer_config(), do: config().mailer_config()

  def cluster_topologies(), do: config().cluster_topologies()

  def object_storage_url_base() do
    port_part =
      case object_storage_port() do
        80 ->
          ""

        port ->
          ":#{port}"
      end

    "#{object_storage_scheme()}#{object_storage_host()}#{port_part}/#{object_storage_bucket()}"
  end

  def object_storage_scheme(), do: config().object_storage_scheme()
  def object_storage_host(), do: config().object_storage_host()
  def object_storage_port(), do: config().object_storage_port()
  def object_storage_bucket(), do: config().object_storage_bucket()
  def object_storage_access_key_id(), do: config().object_storage_access_key_id()
  def object_storage_secret_access_key(), do: config().object_storage_secret_access_key()
end

alias App.RuntimeConfig

[
  {:admin, Legendary.Admin, false},
  {:app, AppWeb, true},
  {:core, Legendary.AuthWeb, false},
  {:content, Legendary.Content, false},
  {:core, Legendary.CoreWeb, false},
  {:object_storage, Legendary.ObjectStorageWeb, false}
]
|> Enum.map(fn {otp_app, module, start_server} ->
  endpoint = Module.concat(module, "Endpoint")
  error_view = Module.concat(module, "ErrorView")

  config otp_app,
         endpoint,
         [
           url: [host: RuntimeConfig.url_host(), port: RuntimeConfig.url_port()],
           http: [
             port: RuntimeConfig.listen_port(),
             transport_options: [socket_opts: [:inet6]]
           ],
           secret_key_base: RuntimeConfig.secret_key_base(),
           render_errors: [view: error_view, accepts: ~w(html json), layout: false],
           pubsub_server: App.PubSub,
           live_view: [signing_salt: RuntimeConfig.live_view_signing_salt()],
           server: start_server && RuntimeConfig.server_command?()
         ] ++ RuntimeConfig.endpoint_extra_opts(otp_app)

  config otp_app, :session_signing_salt, RuntimeConfig.session_signing_salt()
end)

[
  {:admin, Legendary.Admin.Repo},
  {:app, App.Repo},
  {:content, Legendary.Content.Repo},
  {:core, Legendary.Core.Repo},
  {:object_storage, Legendary.ObjectStorage.Repo}
]
|> Enum.map(fn {otp_app, repo} ->
  config otp_app, repo,
    pool: RuntimeConfig.ecto_pool(),
    socket_options: RuntimeConfig.ecto_use_ipv6()

  config otp_app, repo, RuntimeConfig.db_config()
end)

config :core, email_from: RuntimeConfig.email_from()
config :core, Legendary.CoreMailer, RuntimeConfig.mailer_config()

config :mnesia, dir: to_charlist(Path.expand("./priv/mnesia@#{Kernel.node()}"))

config :libcluster,
  topologies: RuntimeConfig.cluster_topologies()

# Object Storage

config :object_storage,
  bucket_name: RuntimeConfig.object_storage_bucket()

config :waffle,
  bucket: RuntimeConfig.object_storage_bucket(),
  asset_host: RuntimeConfig.object_storage_url_base()

config :ex_aws,
  access_key_id: RuntimeConfig.object_storage_access_key_id(),
  secret_access_key: RuntimeConfig.object_storage_secret_access_key()

config :ex_aws, :s3,
  scheme: RuntimeConfig.object_storage_scheme(),
  host: RuntimeConfig.object_storage_host(),
  port: RuntimeConfig.object_storage_port()
