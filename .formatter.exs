plugins =
  if Version.compare(System.version(), "1.13.4") in [:eq, :gt] do
    # Minimum version requirement for the HTMLFormatter
    [Phoenix.LiveView.HTMLFormatter]
  else
    []
  end

[
  plugins: plugins,
  import_deps: [:ecto, :phoenix, :phoenix_live_view],
  inputs: [
    "*.{heex,ex,exs}",
    "priv/*/seeds.exs",
    "{config,lib,test}/**/*.{ex,exs}",
    "apps/*/*.{heex,ex,exs}",
    "apps/*/priv/*/seeds.exs",
    "apps/*/{config,lib,test}/**/*.{ex,exs}"
  ],
  subdirectories: ["apps/**/priv/*/migrations"]
]
