defmodule Legendary.ObjectStorageWeb.UploadView do
  use Legendary.ObjectStorageWeb, :view
  alias Ecto.Changeset
  alias Legendary.ObjectStorage

  import XmlBuilder

  def render("initiate_multipart_upload.xml", %{object: object}) do
    document(
      "InitiateMultipartUploadResult",
      [
        element("Bucket", ObjectStorage.bucket_name()),
        element("Key", object.path),
        element("UploadId", object.id)
      ]
    )
    |> generate()
  end

  def render("error.xml", assigns) do
    errors =
      case assigns do
        %{message: message} ->
          message

        %{changeset: changeset} ->
          changeset.errors
          |> Enum.map_join(", ", fn {key, {message, _}} ->
            "#{key}: #{message}"
          end)
      end

    code = Map.get(assigns, :code, "InvalidArgument")

    path =
      case assigns do
        %{changeset: changeset} ->
          Changeset.get_field(changeset, :path)

        %{path: path} ->
          path
      end

    document(
      "Error",
      [
        element("Code", code),
        element("Message", errors),
        element("Resource", path),
        element("RequestId", "DEADBEEF")
      ]
    )
    |> generate()
  end

  def render("not_found.xml", _assigns) do
    document(
      "Error",
      [
        element("Code", "NoSuchKey")
      ]
    )
    |> generate()
  end
end
