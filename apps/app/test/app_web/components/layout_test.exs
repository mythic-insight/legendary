defmodule AppWeb.LayoutsTest do
  use AppWeb.ConnCase, async: true

  import AppWeb.Layouts

  alias Legendary.Content.Post

  setup %{conn: conn} do
    conn = get(conn, "/session/new")

    %{conn: conn}
  end

  def default_title do
    Legendary.I18n.t!("en", "site.title")
  end

  describe "feed_tag/4" do
    test "for a conn", %{conn: conn} do
      assert stringify(feed_tag(conn, %{})) == ""
    end
  end

  describe "modified_tag/3" do
    test "without a post", %{conn: conn} do
      assert modified_tag(conn, %{}) == nil
    end

    test "with a post", _ do
      post = %Post{modified_gmt: ~N"2023-03-15T00:00:00"}

      assert stringify(modified_tag(nil, nil, %{post: post})) =~ "2023-03-15"
    end
  end

  describe "published_tag/3" do
    test "without a post", %{conn: conn} do
      assert published_tag(conn, %{}) == nil
    end

    test "with a post", _ do
      post = %Post{date_gmt: ~N"2023-03-15T00:00:00"}

      assert stringify(published_tag(nil, nil, %{post: post})) =~ "2023-03-15"
    end
  end

  describe "preview_image_tags/2" do
    test "with post" do
      post = %Post{}

      [
        image,
        twitter_image,
        og_image
      ] = preview_image_tags(nil, %{post: post})

      assert stringify(image) =~ "original.png"
      assert stringify(twitter_image) =~ "original.png"
      assert stringify(og_image) =~ "original.png"
    end

    test "with post with a name" do
      post = %Post{name: "postname"}

      [
        image,
        twitter_image,
        og_image
      ] = preview_image_tags(nil, %{post: post})

      assert stringify(image) =~ "postname/original.png"
      assert stringify(twitter_image) =~ "postname/original.png"
      assert stringify(og_image) =~ "postname/original.png"
    end

    test "without post" do
      assert preview_image_tags(nil, nil) == nil
    end
  end

  describe "title/3" do
    test "for category" do
      assert title(Legendary.Content.PostsView, "show.html", %{post: %{title: "Test"}}) =~
               "Test | #{default_title()}"
    end
  end

  describe "excerpt/3" do
    test "for a post" do
      assert excerpt(Legendary.Content.PostsView, "show.html", %{post: %{excerpt: "Excerpt test"}}) =~
               "Excerpt test"
    end
  end
end
