import fs from "fs"
import path from "path"
import {fileURLToPath} from 'url';
import util from "util"
import {glob} from "glob"
import minimist from "minimist"
import esbuild from "esbuild"
import postcss from "postcss"
import copyStaticFiles from "esbuild-copy-static-files"
import tailwindcss from "tailwindcss"
import autoprefixer from "autoprefixer"
import postcssImport from "postcss-import"
import stylelint from "stylelint"
import cssnano from "cssnano"
import postcssColorFunction from "postcss-color-function"

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const argv = minimist(process.argv.slice(2));

const readFile = util.promisify(fs.readFile);

// config
const ENTRY_FILES = ["js/app.js", "js/admin.js"];
const OUTPUT_DIR = path.resolve(__dirname, "../priv/static/");
const MODE = process.env["NODE_ENV"] || "production";
const TARGET = "es2016";
const postcssPlugins = [
  postcssImport({
    plugins: [stylelint()],
  }),
  tailwindcss(path.resolve(__dirname, "tailwind.config.js")),
  autoprefixer,
  cssnano({
    preset: "default",
  }),
  postcssColorFunction(),
];

const heexPurgeWatcherPlugin = () => ({
  name: "heexWatch",
  setup: (build) => {
    build.onResolve({ filter: /\.css/ }, async (args) => {
      const watchFiles = await glob("../../../**/*.heex")

      return {watchFiles: watchFiles}
    })
  }
})

const fontawesomePlugin = () => ({
  name: "fontawesome",
  setup: (build) => {
    build.onResolve({ filter: /\.\.\/webfonts\// }, async (args) => {
      const resolved = args.path.replace(
        "../webfonts/",
        "node_modules/@fortawesome/fontawesome-free/webfonts/"
      );
      return {
        path: path.resolve(__dirname, resolved),
      };
    });
  },
});

const postcssPlugin = (options = { plugins: [] }) => ({
  name: "postcss",
  setup: function (build) {
    build.onLoad({ filter: /.\.(css)$/, namespace: "file" }, async (args) => {
      const cssIn = await readFile(args.path);

      const { css } = await postcss(options.plugins).process(cssIn, {
        from: args.path,
      });

      return {
        contents: css,
        loader: "css",
        resolveDir: path.dirname(args.path),
      };
    });
  },
});

const watchPlugin = {
  name: 'watch-plugin',
  setup(build) {
    build.onEnd((_result) => {
      console.log(`[+] Rebuilt ${ENTRY_FILES} to ${OUTPUT_DIR}.`)
    })
  }
}

// build
async function build(watchFlag) {
  console.log(
    `[+] Starting static assets build with esbuild. Build mode ${MODE}...`
  );
  let ctx = await esbuild.context({
      entryPoints: ENTRY_FILES,
      outdir: OUTPUT_DIR,
      minify: MODE === "dev" || MODE === "development" ? false : true,
      bundle: true,
      target: TARGET,
      loader: {
        // built-in loaders: js, jsx, ts, tsx, css, json, text, base64, dataurl, file, binary
        ".ttf": "file",
        ".otf": "file",
        ".svg": "file",
        ".eot": "file",
        ".woff": "file",
        ".woff2": "file",
        ".svg": "file",
      },
      plugins: [
        postcssPlugin({
          plugins: postcssPlugins,
        }),
        fontawesomePlugin(),
        heexPurgeWatcherPlugin(),
        copyStaticFiles({
          dest: "../priv/static/",
        }),
        watchPlugin
      ],
      define: {
        "process.env.NODE_ENV":
          MODE === "dev" || MODE === "development"
            ? '"development"'
            : '"production"',
        global: "window",
      },
      sourcemap: MODE === "dev" || MODE === "development" ? true : false,
    });

    if(watchFlag) {
      await watch(ctx)
    } else {
      await rebuild(ctx)
    }
}

async function watch(ctx) {
  let result = await ctx.watch()
  console.log("[+] Watching...")
  return result
}

async function rebuild(ctx) {
  await ctx.rebuild()
  await ctx.dispose()
}

// helpers
function mkDirSync(dir) {
  if (fs.existsSync(dir)) {
    return;
  }

  try {
    fs.mkdirSync(dir);
  } catch (err) {
    if (err.code === "ENOENT") {
      mkDirSync(path.dirname(dir));
      mkDirSync(dir);
    }
  }
}

// make sure build directory exists
mkDirSync(OUTPUT_DIR);

// build initial
await build(argv.watch);
