defmodule App.Release do
  @moduledoc """
  Used for executing DB release tasks when run in production without Mix
  installed.
  """
  require Logger

  @apps ~w(admin app content core object_storage)a

  def migrate do
    load_apps()

    for repo <- repos() do
      Logger.info("Migrating #{repo}...")
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end

  def rollback(repo, version) do
    load_apps()
    {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :down, to: version))
  end

  defp repos do
    Enum.flat_map(@apps, fn app ->
      Application.fetch_env!(app, :ecto_repos)
    end)
  end

  defp load_apps do
    for app <- @apps do
      Application.load(app)
    end
  end
end
