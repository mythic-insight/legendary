defmodule Legendary.Admin.ErrorHelpersTest do
  use Legendary.Admin.ConnCase

  import Legendary.Admin.ErrorHelpers

  test "error_tag/2" do
    markup =
      with_form(fn f ->
        error_tag(f, :error_field)
      end)
      |> stringify()

    assert markup =~ "invalid-feedback"
  end

  test "error_tag/3" do
    markup =
      with_form(fn f ->
        error_tag(f, :error_field, class: "test-class")
      end)
      |> stringify()

    assert markup =~ "test-class"
  end

  test "translate_error/1" do
    assert translate_error({"is an error", []}) == "is an error"
    assert translate_error({"%{count} errors", %{count: 3}}) == "3 errors"
  end
end
