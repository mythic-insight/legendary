# Phoenix LiveView

Legendary comes with pre-styled templates for Phoenix LiveView.
Running `mix phx.live.gen` will result in a styled LiveView resource.

Please note that Legendary comes with an extended `apps/app/lib/app_web/live/live_helpers.ex` file (mainly authentication)
and accompanying tests in `apps/app/test/app_web/live_helpers_test.exs`.

If you don't need and LiveView functionality at all you can remove both files and the import in `apps/app/lib/app_web.ex`:

``` elixir
  defp view_helpers do
    quote do
      # Use all HTML functionality (forms, tags, etc)
      use Phoenix.HTML

      # Import LiveView and .heex helpers (live_render, live_patch, <.form>, etc)
      import Phoenix.LiveView.Helpers
      import AppWeb.LiveHelpers  # <=========== Remove this line

      # Import basic rendering functionality (render, render_layout, etc)
      import Phoenix.View

      import AppWeb.ErrorHelpers
      import AppWeb.Gettext
      import Legendary.CoreWeb.Helpers
      alias AppWeb.Router.Helpers, as: Routes
    end
  end
  ```

  If you want to re-add LiveView support again you can simply run `mix phx.gen.live`.
  This recreates `apps/app/lib/app_web/live/live_helpers.ex` and re-injects the import into `apps/app/lib/app_web.ex`.
  It will not however re-create the test file `apps/app/test/app_web/live_helpers_test.exs`.