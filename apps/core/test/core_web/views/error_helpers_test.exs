defmodule Legendary.CoreWeb.ErrorHelpersTest do
  use Legendary.Core.DataCase

  import Legendary.CoreWeb.ErrorHelpers

  test "error_class/2" do
    assert with_form(fn f -> error_class(f, :error_field) end) |> stringify() =~ "error"
    refute with_form(fn f -> error_class(f, :no_error_field) end) |> stringify()  =~ "error"
  end

  test "error_tag/3" do
    markup =
      with_form(fn f ->
        error_tag(f, :error_field, class: "foobar")
      end)
      |> stringify()

    assert markup =~ "foobar"
    assert markup =~ "span"
    assert markup =~ "is an error"
  end
end
