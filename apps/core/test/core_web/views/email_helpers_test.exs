defmodule Legendary.CoreWeb.EmailHelpersTest do
  use Legendary.Core.DataCase

  import Legendary.CoreWeb.EmailHelpers

  test "framework_styles/0" do
    assert %{background: _, body: _} = framework_styles()
  end

  test "framework_styles/1" do
    assert framework_styles().background == framework_styles(:background)
  end

  test "application_styles/1" do
    assert %{} = application_styles(:background)
  end

  test "effective_styles/2" do
    assert %{color: "#FFF"} = effective_styles(:test, %{color: "#FFF"})

    assert Map.merge(framework_styles(:background), %{color: "#FFF"}) ==
             effective_styles(:background, %{color: "#FFF"})
  end

  test "preview/1" do
    safe = preview(do: "test preview")
    assert stringify(safe) =~ "<div"
  end

  test "header/1" do
    safe = header(do: "Test Header!")

    assert stringify(safe) =~ "Test Header!"
  end

  test "spacer/0" do
    assert stringify(spacer()) =~ "&nbsp;"
  end

  test "row/1" do
    assert stringify(row(do: "Row test")) =~ "Row test"
  end

  test "col/3" do
    safe = col(1, [of: 2], do: "Column test")

    assert stringify(safe) =~ "Column test"
    assert stringify(safe) =~ "width=\"50.0%\""
  end

  test "hero_image/1" do
    safe = hero_image(src: "src.jpg")

    assert stringify(safe) =~ "src.jpg"
    assert stringify(safe) =~ "<img"
  end

  test "h1/1" do
    safe = h1(do: "H1 Test!")

    assert stringify(safe) =~ "H1 Test!"
  end

  test "h2/1" do
    safe = h2(do: "H2 Test!")

    assert stringify(safe) =~ "H2 Test!"
  end

  test "p/1" do
    safe = p(do: "This is the test paragraph.")

    assert stringify(safe) =~ "This is the test paragraph."
  end

  test "button/2" do
    safe = button([href: "http://duckduckgo.com"], do: "Button text!")

    assert stringify(safe) =~ "duckduckgo.com"
    assert stringify(safe) =~ "Button text!"
  end

  test "ul/1" do
    markup = stringify(ul(items: ["First item", "Second item"]))

    assert markup =~ "<li"
    assert markup =~ "First item"
    assert markup =~ "Second item"
  end

  test "footer/0" do
    markup = stringify(footer())

    assert markup =~ Legendary.I18n.t!("en", "email.company.name")
  end
end
