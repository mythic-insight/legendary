defmodule Legendary.AuthWeb.CoreComponents do
  @moduledoc """
  Provides core UI components.

  The components in this module use Tailwind CSS, a utility-first CSS framework.
  See the [Tailwind CSS documentation](https://tailwindcss.com) to learn how to
  customize the generated components in this module.

  Icons are provided by [heroicons](https://heroicons.com), using the
  [heroicons_elixir](https://github.com/mveytsman/heroicons_elixir) project.
  """
  use Phoenix.Component

  import Legendary.CoreWeb.ErrorHelpers

  alias Phoenix.HTML.Form

  @datetime_default_options [
    month: [
      class: "appearance-none border-b-2 border-dashed",
      options: [
        {"Jan", "1"},
        {"Feb", "2"},
        {"Mar", "3"},
        {"Apr", "4"},
        {"May", "5"},
        {"Jun", "6"},
        {"Jul", "7"},
        {"Aug", "8"},
        {"Sep", "9"},
        {"Oct", "10"},
        {"Nov", "11"},
        {"Dec", "12"}
      ]
    ],
    day: [class: "appearance-none border-b-2 border-dashed"],
    year: [class: "appearance-none border-b-2 border-dashed"],
    hour: [class: "appearance-none border-b-2 border-dashed"],
    minute: [class: "appearance-none border-b-2 border-dashed"],
    second: [class: "appearance-none border-b-2 border-dashed"]
  ]

  @general_input_class "px-4 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"

  attr :extension, :atom

  slot :inner_block

  def for_pow_extension(assigns) do
    {extensions, _rest} = Application.get_env(:core, :pow) |> Keyword.pop(:extensions, [])
    assigns = Map.put(assigns, :extensions, extensions)

    ~H"""
    <%= if Enum.any?(@extensions, &(&1 == @extension)) do %>
      <%= render_slot(@inner_block || []) %>
    <% else %>
    <% end %>
    """
  end

  slot :inner_block

  def floating_page_wrapper(assigns) when is_map(assigns) do
    ~H"""
    <section class="absolute w-full h-full">
      <div class="absolute top-0 w-full h-full bg-gray-700"></div>
      <div class="container mx-auto px-4 h-full">
        <div class="flex content-center items-center justify-center h-full">
          <div class="w-full lg:w-4/12 px-4">
            <%= render_slot(@inner_block || []) %>
          </div>
        </div>
      </div>
    </section>
    """
  end

  attr :changeset, :map, required: true
  attr :title, :string

  slot :inner_block, required: true

  def floating_form(assigns) do
    ~H"""
    <h1 class="relative text-white text-xl font-semibold text-center pb-6"><%= @title %></h1>
    <div class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300 border-0">
      <.changeset_error_block changeset={@changeset} />
      <div class="flex-auto px-4 lg:px-10 pt-6 pb-10">
        <%= render_slot(@inner_block || []) %>
      </div>
    </div>
    """
  end

  attr :changeset, :any, required: true

  def changeset_error_block(assigns) do
    ~H"""
    <%= if @changeset.action do %>
      <div class="flex-auto px-4 lg:px-10 py-6 text-red-500 text-center">
        <p>Oops, something went wrong! Please check the errors below.</p>
      </div>
    <% end %>
    """
  end

  attr :field, :any, required: true
  attr :value, :any
  attr :rest, :global

  def hidden_input(assigns) do
    ~H"""
    <input type="hidden" name={@field.name} id={@field.id} value={@field.value} {@rest} />
    """
  end

  attr :form, :any, required: true, doc: "the form that the input belongs to"
  attr :class, :any, default: ""
  attr :error_class, :string
  attr :field, :atom, required: true
  attr :input_helper, :atom, default: nil
  attr :label_text, :string, default: nil
  attr :options, :map
  attr :type, :string, default: "text"
  attr :opts, :global, include: ~w(autocomplete placeholder)

  slot :inner_block,
    doc: "the optional inner block that renders that renders under the input itself"

  def styled_input(%{input_helper: input_helper, form: form, field: field} = assigns) do
    assigns = %{assigns | input_helper: input_helper || Form.input_type(form, field)}

    ~H"""
    <div class={"relative w-full mb-3 #{error_class(@form, @field)}"}>
      <%= if @label_text do %>
        <%= Form.label(@form, @field, @label_text,
          class: "block uppercase text-gray-700 text-xs font-bold mb-2"
        ) %>
      <% else %>
        <%= Form.label(@form, @field, class: "block uppercase text-gray-700 text-xs font-bold mb-2") %>
      <% end %>

      <%= if @type in [:date_select, :time_select, :datetime_select] do %>
        <.styled_datetime_input
          form={@form}
          field={@field}
          type={@type}
          class={[default_class_for_type(@type), @class]}
          {@opts}
        />
      <% else %>
        <%= if @input_helper in [:select, :multiple_select] do %>
          <.styled_select_input
            form={@form}
            field={@field}
            options={@options}
            input_helper={@input_helper}
            class={[default_class_for_type(@type), @class]}
            {@opts}
          />
        <% else %>
          <.styled_input_inner
            form={@form}
            field={@field}
            input_helper={@input_helper}
            class={[default_class_for_type(@type), @class]}
            {@opts}
          />
        <% end %>
      <% end %>

      <%= render_slot(@inner_block || []) %>

      <%= error_tag(@form, @field, class: "text-red-500 italic") %>
    </div>
    """
  end

  defp default_class_for_type(type)
       when type in [:date_select, :time_select, :datetime_select] do
    "bg-white shadow rounded p-3"
  end

  defp default_class_for_type(:checkbox),
    do:
      "appearance-none h-10 w-10 bg-white checked:bg-gray-500 rounded shadow focus:outline-none focus:ring text-white text-xl font-bold mb-2"

  defp default_class_for_type(_),
    do:
      "px-4 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"

  attr :type, :atom, required: true
  attr :field, :atom, required: true
  attr :form, :any, required: true
  attr :class, :any, default: @general_input_class
  attr :child_opts, :list, default: @datetime_default_options
  attr :opts, :global

  def styled_datetime_input(assigns) do
    ~H"""
    <div class={@class}>
      <%= apply(Phoenix.HTML.Form, @type, [@form, @field, Keyword.new(@opts) ++ @child_opts]) %>
    </div>
    """
  end

  attr :input_helper, :atom, required: true
  attr :form, :any, required: true
  attr :field, :atom, required: true
  attr :class, :any, default: @general_input_class
  attr :options, :list, required: true
  attr :opts, :global

  def styled_select_input(assigns) do
    ~H"""
    <%= apply(Phoenix.HTML.Form, @input_helper, [
      @form,
      @field,
      @options,
      Keyword.new(@opts) ++ [class: @class]
    ]) %>
    """
  end

  attr :input_helper, :atom, required: true
  attr :form, :any, required: true
  attr :field, :atom, required: true
  attr :class, :any, default: @general_input_class
  attr :opts, :global

  def styled_input_inner(assigns) do
    ~H"""
    <%= apply(Phoenix.HTML.Form, @input_helper, [
      @form,
      @field,
      Keyword.new(@opts) ++ [class: @class]
    ]) %>
    """
  end

  attr :opts, :global

  slot :inner_block, required: true

  def styled_button(assigns) do
    ~H"""
    <button
      {@opts}
      type="submit"
      class="bg-gray-900 text-white active:bg-gray-700 text-sm font-bold uppercase px-4 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
    >
      <%= render_slot(@inner_block) %>
    </button>
    """
  end

  attr :opts, :global

  slot :inner_block, required: true

  def styled_button_link(assigns) do
    ~H"""
    <.link
      {@opts}
      class="bg-gray-900 text-white active:bg-gray-700 text-sm font-bold uppercase px-4 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
    >
      <%= render_slot(@inner_block) %>
    </.link>
    """
  end

  attr :opts, :map
  attr :to, :string

  slot :inner_block, required: true

  def styled_button_live_patch(assigns) do
    ~H"""
    <.link
      patch={@to}
      {@opts}
      class="bg-gray-900 text-white active:bg-gray-700 text-sm font-bold uppercase px-4 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
    >
      <%= render_slot(@inner_block || []) %>
    </.link>
    """
  end

  attr :opts, :map
  attr :to, :string

  slot :inner_block, required: true

  def styled_button_live_redirect(assigns) do
    ~H"""
    <.link
      navigate={@to}
      {@opts}
      class="bg-gray-900 text-white active:bg-gray-700 text-sm font-bold uppercase px-4 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
    >
      <%= render_slot(@inner_block || []) %>
    </.link>
    """
  end
end
