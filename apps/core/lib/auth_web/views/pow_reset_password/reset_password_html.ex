defmodule Legendary.AuthWeb.PowResetPassword.ResetPasswordHTML do
  @moduledoc """
  HTML module for password resets.
  """
  use Legendary.AuthWeb, :html

  embed_templates "reset_password/*", root: __DIR__
end
