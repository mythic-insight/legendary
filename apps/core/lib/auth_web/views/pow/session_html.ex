defmodule Legendary.AuthWeb.Pow.SessionHTML do
  @moduledoc """
  HTML module for sign in form.
  """
  use Legendary.AuthWeb, :html

  embed_templates "session/*", root: __DIR__
end
