defmodule Legendary.AuthWeb.Pow.RegistrationHTML do
  @moduledoc """
  HTML module for Registration form.
  """
  use Legendary.AuthWeb, :html

  embed_templates "registration/*", root: __DIR__
end
