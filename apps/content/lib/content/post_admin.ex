defmodule Legendary.Content.PostAdmin do
  @moduledoc """
  Custom admin logic for content posts and pages.
  """

  alias Legendary.Content.{Post, Posts.PreviewImages}

  import Ecto.Query, only: [from: 2]

  def singular_name(_) do
    "Post or Page"
  end

  def plural_name(_) do
    "Posts and Pages"
  end

  def create_changeset(schema, attrs) do
    schema
    |> Post.changeset(attrs)
    |> PreviewImages.handle_preview_image_upload(attrs)
  end

  def update_changeset(schema, attrs) do
    schema
    |> Post.changeset(attrs)
    |> PreviewImages.handle_preview_image_upload(attrs)
  end

  def index(_) do
    [
      id: nil,
      type: nil,
      name: nil,
      title: nil,
      status: nil,
      date_gmt: nil,
      modified_gmt: nil
    ]
  end

  def form_fields(_) do
    authors_query =
      from u in Legendary.Auth.User,
        where: "admin" in u.roles,
        select: [u.email, u.id]

    authors =
      authors_query
      |> Legendary.Content.Repo.all()
      |> Enum.map(fn [email, id] ->
        {email, id}
      end)

    [
      type: %{choices: [{"Blog Post", :post}, {"Page", :page}]},
      name: %{label: "Slug"},
      title: nil,
      content: %{type: :textarea, rows: 32},
      status: %{
        choices: choices_with_default(:status, [{"Publish", :publish}, {"Draft", :draft}])
      },
      author_id: %{choices: authors},
      excerpt: %{type: :textarea, rows: 4},
      sticky: nil,
      comment_status: %{
        choices: choices_with_default(:comment_status, [{"open", :open}, {"closed", :closed}])
      },
      ping_status: %{
        choices: choices_with_default(:ping_status, [{"open", :open}, {"closed", :closed}])
      },
      menu_order: nil,
      social_media_preview_image: %{type: :hidden}
    ]
  end

  # Kaffy doesn't support default value, move the default value as a first element of the select list
  defp choices_with_default(field_name, values) do
    field_value = Application.get_env(:content, :post_admin)[:default][field_name]
    Enum.uniq([List.keyfind(values, field_value, 0) | values])
  end
end
