defmodule Legendary.Content.Posts.Static do
  @moduledoc """
  A View which compiles heex templates from the static_pages directory and makes them available to
  the content engine.
  """

  @static_page_directory_path Path.expand(
                                "../../../../app/lib/app_web/templates/static_pages",
                                __DIR__
                              )
  @template_paths Path.wildcard(Path.join(@static_page_directory_path, "**/*.html.heex"))

  use Phoenix.Component

  alias Phoenix.LiveView.HTMLEngine
  alias Phoenix.LiveView.TagEngine
  import Legendary.CoreWeb.Helpers

  for template_path <- @template_paths do
    slug = template_path |> Path.basename(".heex") |> Path.basename(".html")
    quoted = HTMLEngine.compile(template_path, slug)

    def compiled_static(unquote(slug)) do
      assigns = %{}

      unquote(quoted)
      |> Phoenix.HTML.html_escape()
    end
  end

  def static(slug) do
    template_path = Path.join([@static_page_directory_path, "#{slug}.html.heex"])
    trim = Application.get_env(:phoenix, :trim_on_html_eex_engine, true)

    case File.read(template_path) do
      {:ok, source} ->
        EEx.eval_string(source, [assigns: %{}],
          engine: TagEngine,
          line: 1,
          file: template_path,
          trim: trim,
          source: source,
          caller: __ENV__,
          functions: [
            {Phoenix.Component, Phoenix.Component.__info__(:functions)},
            {Legendary.CoreWeb.Helpers, Legendary.CoreWeb.Helpers.__info__(:functions)}
          ],
          tag_handler: HTMLEngine
        )

      {:error, _} ->
        nil
    end
  end

  def compiled_static(_), do: nil

  if Mix.env() == :dev do
    def render(template_name, _conn) do
      static(Path.basename(template_name, ".html"))
    end
  else
    def render(template_name, _conn) do
      compiled_static(Path.basename(template_name, ".html"))
    end
  end
end
