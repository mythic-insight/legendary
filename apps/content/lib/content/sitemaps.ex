defmodule Legendary.Content.Sitemaps do
  @moduledoc """
    This module generates sitemaps for the website and pings search engines as
    appropriate.
  """
  use Legendary.Content, :verified_routes
  alias Legendary.Content.{Endpoint, Post, Posts, Repo, Terms}
  import Ecto.Query

  require Logger
  require Sitemap.DSL

  use Oban.Worker

  @impl Oban.Worker
  def perform(_job) do
    generate()
  end

  @spec generate :: :ok
  def generate do
    config = [
      host: "https://#{Application.get_env(:content, Endpoint)[:url][:host]}",
      files_path: "tmp/sitemap/",
      public_path: "",
      adapter: Legendary.Content.SitemapStorage
    ]

    import Sitemap.Generator, only: [add: 2, fin: 0, ping: 0]

    Sitemap.Config.update(config)

    add("", priority: 0.5, changefreq: "hourly", expires: nil)

    posts =
      Posts.post_scope()
      |> where([p], p.type not in ["nav_menu_item", "attachment"])
      |> Repo.all()

    for post <- posts do
      add(~p"/#{post}",
        priority: 0.5,
        changefreq: "hourly",
        expires: nil
      )

      page_count = Post.content_page_count(post)

      if page_count > 1 do
        2..page_count
        |> Enum.each(fn page ->
          add(
            ~p"/#{post}?page=#{page}",
            priority: 0.5,
            changefreq: "hourly",
            expires: nil
          )
        end)
      end
    end

    Terms.categories()
    |> Repo.all()
    |> Enum.each(fn category ->
      add(~p"/category/#{category}",
        priority: 0.5,
        changefreq: "hourly",
        expires: nil
      )
    end)

    # notify search engines (currently Google and Bing) of the updated sitemap
    if Mix.env() == :prod, do: ping()

    fin()

    Logger.info("Sitemap generated.")

    :ok
  end
end
