defmodule Legendary.Content.PostTopmatterComponent do
  @moduledoc """
  Live Component for post top matter (such as author details and timestamps).
  """

  use Legendary.Content, :live_component

  def render(assigns) do
    ~H"""
    <div class="flex w-full items-center font-sans py-6">
      <%= if @author.homepage_url do %>
        <.link href={@author.homepage_url} rel="author" class="p-author h-card">
          <%= img_tag(gravatar_url_for_email(@author.email),
            alt: "Photo of #{@author.display_name}",
            class: "Gravatar u-photo w-10 h-10 rounded-full mr-4"
          ) %>
        </.link>
      <% else %>
        <p class="p-author h-card">
          <%= img_tag(gravatar_url_for_email(@author.email),
            alt: "Photo of #{@author.display_name}",
            class: "Gravatar u-photo w-10 h-10 rounded-full mr-4"
          ) %>
        </p>
      <% end %>
      <div class="flex-1 px-2">
        <p class="text-base font-bold text-base md:text-xl leading-none mb-2">
          <%= @author.display_name %>
        </p>
        <%= if @single do %>
          <p class="text-gray-600 text-xs md:text-base">
            Published
            <time class="dt-published" datetime={@post.date}>
              <%= @post.date |> Timex.format!("%F", :strftime) %>
            </time>
          </p>
        <% else %>
          <.link patch={~p"/posts/#{@post}"}>
            <p class="text-gray-600 text-xs md:text-base">
              Published
              <time class="dt-published" datetime={@post.date}>
                <%= @post.date |> Timex.format!("%F", :strftime) %>
              </time>
            </p>
          </.link>
        <% end %>
      </div>
    </div>
    """
  end
end
