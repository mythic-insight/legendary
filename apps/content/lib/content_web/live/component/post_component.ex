defmodule Legendary.Content.PostComponent do
  @moduledoc """
  Live Component for a single post.
  """
  use Legendary.Content, :live_component

  alias Legendary.Content.Post

  alias Legendary.Content.{
    PostTopmatterComponent,
    ThumbComponent,
    CommentsComponent,
    ReplyFormComponent,
    PostPageLive
  }

  def update(assigns, socket) do
    socket = assign(socket, assigns)
    {:ok, socket}
  end

  def handle_event("submit_password", %{"post_password" => %{"password" => password}}, socket) do
    socket = assign(socket, :password, password)

    {:noreply, socket}
  end

  attr :post, Post, required: true
  attr :current_page, :integer, default: 1

  def post_pagination(assigns) do
    ~H"""
    <div>
      <%= if Legendary.Content.Post.paginated_post?(@post) do %>
        <nav class="paginator">
          Page:
          <%= for page <- 1..Legendary.Content.Post.content_page_count(@post) do %>
            <.pagination_link post={@post} page={page} current_page={@current_page} />
          <% end %>
        </nav>
      <% end %>
    </div>
    """
  end

  defp pagination_link(assigns) do
    ~H"""
    <%= if @current_page == nil || @current_page != @page do %>
      <.link patch={~p"/posts/#{@post}?page=#{@page}"}>
        <%= @page %>
      </.link>
    <% else %>
      <span class="paginator-page"><%= @page %></span>
    <% end %>
    """
  end

  def render(%{post: %{password: post_password}} = assigns)
      when post_password != nil and byte_size(post_password) > 0 do
    case assigns do
      %{password: password} when password == post_password ->
        render_authenticated(assigns)

      _ ->
        render_password_form(assigns)
    end
  end

  def render(assigns) do
    render_authenticated(assigns)
  end

  def render_password_form(assigns) do
    ~H"""
    <div>
      <article class="max-w-xl mx-auto h-entry py-12 post post-#{@post.id} #{if @post.stick, do: 'sticky'}">
        <div class="text-4xl font-bold">
          <.link patch={Routes.live_path(@socket, PostPageLive, @post.name)} class="u-url">
            <%= @post.title %>
          </.link>
        </div>
        <div class="#{if @post.format, do: @post.format.slug} e-content py-12">
          <%= floating_form "", %{action: nil} do %>
            <h1 class="relative text-xl font-semibold text-center pb-6">
              Password Protected Post
            </h1>

            <.form
              :let={f}
              phx-submit="submit_password"
              phx-target={@myself}
              for={%{}}
              as={:post_password}
            >
              <%= styled_input(f, :password, label: "Enter Password to View Post") %>
              <%= styled_button("Submit", phx_disable_with: "Submitting...", phx_debounce: "blur") %>
            </.form>
          <% end %>
        </div>
      </article>
    </div>
    """
  end

  def render_authenticated(assigns) do
    ~H"""
    <div>
      <article class="max-w-xl mx-auto h-entry py-12 post post-#{@post.id} #{if @post.stick, do: 'sticky'}">
        <div>
          <h1 class="text-4xl font-bold">
            <.link patch={~p"/#{@post}"} class="u-url">
              <%= @post.title %>
            </.link>
          </h1>
          <.live_component
            module={PostTopmatterComponent}
            id={"topmatter-#{post_element_suffix(@post)}"}
            post={@post}
            author={author(@post)}
            single={true}
          />
        </div>
        <div class="#{if @post.format, do: @post.format.slug} e-content py-12 Article-content">
          <.live_component
            module={ThumbComponent}
            id={"thumb-#{post_element_suffix(@post)}"}
            post={@post}
            thumbs={@thumbs}
          />
          <div class="Article-content-words">
            <%= if @single do %>
              <%= @post |> Legendary.Content.Post.content_page(@page) |> process_content |> raw %>
            <% else %>
              <%= @post
              |> Legendary.Content.Post.content_page(1)
              |> Legendary.Content.Post.before_more()
              |> process_content
              |> raw %>
              <%= if @post.content =~ "<!--more-->" do %>
                <p>
                  <.link patch={Routes.live_path(@socket, PostPageLive, @post.name)}>
                    Keep Reading
                  </.link>
                </p>
              <% end %>
            <% end %>
            <.post_pagination post={@post} current_page={@page} />
          </div>
          <%= case @post.categories || [] do %>
            <% [] -> %>
              <%= "" %>
            <% categories -> %>
              <div class="flex pt-6">
                <h3 class="text-xl font-bold mr-4">Categories</h3>
                <%= for term <- categories do %>
                  <.link
                    patch={~p"/category/#{term.slug}"}
                    class="rounded-full bg-gray-300 px-4 py-1 mr-2"
                  >
                    <%= term.name %>
                  </.link>
                <% end %>
              </div>
          <% end %>
        </div>
      </article>
      <%= if @single && @post.comment_status == "open" do %>
        <div class="w-full bg-gray-800 py-12">
          <div class="max-w-xl mx-auto">
            <h3 class="text-3xl text-white">Comments</h3>
            <.live_component
              module={CommentsComponent}
              id={"comment-list-#{post_element_suffix(@post)}"}
              post={@post}
              parent_id={0}
            />
            <.live_component
              module={ReplyFormComponent}
              id={"comment-form-#{post_element_suffix(@post)}"}
              post={@post}
              ip_address={@ip_address}
            />
          </div>
        </div>
      <% end %>
    </div>
    """
  end
end
