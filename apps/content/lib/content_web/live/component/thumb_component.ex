defmodule Legendary.Content.ThumbComponent do
  @moduledoc """
  Post thumbnail component.
  """
  use Legendary.Content, :live_component

  def render(assigns) do
    ~H"""
    <div>
      <%= case @thumbs[@post.id] do %>
        <% thumb = %Legendary.Content.Post{} -> %>
          <%= if thumb && thumb |> Legendary.Content.Attachment.vertical?() do %>
            <div class="post-thumbnail post-thumbnail--vertical">
              <%= img_tag(thumb.guid) %>
            </div>
          <% else %>
            <div class="post-thumbnail">
              <%= img_tag(thumb.guid) %>
            </div>
          <% end %>
        <% nil -> %>
      <% end %>
    </div>
    """
  end
end
