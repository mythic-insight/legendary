defmodule Legendary.Content.ReplyFormComponent do
  @moduledoc """
  Page reply form component.
  """
  use Legendary.Content, :live_component

  alias Legendary.Content.Comments
  alias Legendary.Content.Comment
  alias Legendary.Content.Post
  alias Legendary.Content.Repo

  import Ecto.Query

  def mount(socket) do
    changeset = Comments.change_comment(%Comment{})

    # comment_changeset =  comment_changeset_for_post(@post)

    {:ok, assign(socket, changeset: changeset)}
  end

  def update(%{post: post, ip_address: ip_address} = _assigns, socket) do
    # changeset = Comments.change_comment(%Comment{})

    changeset = comment_changeset_for_post(post)

    {:ok, assign(socket, changeset: changeset, ip_address: ip_address)}
  end

  def render(assigns) do
    ~H"""
    <div class="mt-12">
      <%= floating_form "Leave My Comment", @changeset do %>
        <.form :let={f} for={@changeset} phx-submit="save" phx-target={@myself}>
          <.input type="hidden" field={{f, :parent}} />
          <.input type="hidden" field={{f, :post_id}} />
          <%= styled_input(f, :author, label: "Your Name") %>
          <%= styled_input(f, :author_email, label: "Your Email") %>
          <%= styled_input(f, :author_url, label: "Your Website") %>
          <%= styled_input(f, :content, label: "Comment", input_helper: :textarea) %>
          <%= styled_button("Leave My Comment", phx_disable_with: "Saving...", phx_debounce: "blur") %>
        </.form>
      <% end %>
    </div>
    """
  end

  defp comment_changeset_for_post(%Post{} = post) do
    %Comment{
      post_id: post.id
    }
    |> Comment.changeset()
  end

  def handle_event("save", %{"comment" => comment_params}, socket) do
    comment_params =
      comment_params
      |> Map.merge(%{
        "comment_author_IP" => to_string(:inet_parse.ntoa(socket.assigns.ip_address))
      })

    case Comments.create_comment(comment_params) do
      {:ok, comment} ->
        changeset = Comments.change_comment(%Comment{})

        post =
          Post
          |> where([p], p.id == ^comment.post_id)
          |> Repo.one()

        {:noreply,
         socket
         |> put_flash(:info, "Comment created successfully.")
         |> assign(changeset: changeset)
         |> push_navigate(to: Routes.live_path(socket, Legendary.Content.PostPageLive, post))}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  def handle_event("validate", %{"comment" => comment_params}, socket) do
    changeset =
      %Comment{}
      |> Comments.change_comment(comment_params)
      |> Map.put(:action, :insert)

    {:noreply, assign(socket, changeset: changeset)}
  end
end
