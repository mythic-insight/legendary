defmodule Legendary.Content.PostPageLive do
  @moduledoc """
  LiveView for a single post page.
  """
  use Legendary.Content, :live_view

  alias Legendary.Content.Options
  alias Legendary.Content.PostComponent
  alias Legendary.Content.Posts
  alias Legendary.Content.Posts.Static

  def mount(%{"id" => id} = params, session, socket) do
    page = Map.get(params, "page", "1") |> String.to_integer()

    socket = assign_defaults(socket, session)
    post = Posts.get_post(id)

    socket =
      socket
      |> assign_post(post, id)
      |> assign(:page, page)
      |> assign(:ip_address, get_ip_address(socket))

    {:ok, socket}
  end

  def mount(_, %{"post" => post} = session, socket) do
    page = Map.get(session, "page", "1") |> String.to_integer()

    socket = assign_defaults(socket, session)

    socket =
      socket
      |> assign_post(post, post.id)
      |> assign(:page, page)
      |> assign(:ip_address, get_ip_address(socket))

    {:ok, socket}
  end

  def handle_params(%{"id" => id}, _url, socket) do
    socket =
      if same_id?(socket, id) do
        socket
      else
        post = Posts.get_post(id)
        assign_post(socket, post, id)
      end

    {:noreply, socket}
  end

  def handle_params(%{}, url, socket) do
    page_id = Options.get_value("page_on_front") || "index"

    handle_params(%{"id" => page_id}, url, socket)
  end

  defp same_id?(%{assigns: %{post: post}}, id) when not is_nil(post),
    do: post.id == id || post.name == id

  defp same_id?(_, _), do: false

  defp assign_post(socket, nil, post_id) do
    case get_static(post_id) do
      nil ->
        raise Ecto.NoResultsError, queryable: Posts.get_post_scope(post_id)

      content ->
        socket
        |> assign(:static_content, content)
        |> assign(:static_slug, post_id)
        |> assign(:post, nil)
    end
  end

  defp assign_post(socket, post, _post_id) do
    thumbs = Posts.thumbs_for_posts([post])

    assign(socket, page_title: post.title, post: post, thumbs: thumbs)
  end

  if Mix.env() == :dev do
    defp get_static(post_id), do: Static.static(post_id)
  else
    defp get_static(post_id), do: Static.compiled_static(post_id)
  end

  def render(%{static_content: _} = assigns) do
    ~H"""
    <%= @static_content %>
    """
  end

  def render(assigns) do
    ~H"""
    <.live_component
      module={PostComponent}
      id={"post-single-#{@post.id}"}
      post={@post}
      thumbs={@thumbs}
      page={@page}
      single={true}
      ip_address={@ip_address}
    />

    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/highlight.min.js">
    </script>
    <script
      charset="UTF-8"
      src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/languages/elixir.min.js"
    >
    </script>
    <script>
      hljs.initHighlightingOnLoad();
    </script>
    """
  end
end
