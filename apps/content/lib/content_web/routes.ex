defmodule Legendary.Content.Routes do
  @moduledoc """
  Routes for the content engine, including blog posts, feeds, and pages.
  """

  defmacro __using__(_opts \\ []) do
    quote do
      pipeline :feed do
        plug :accepts, ["rss"]
        plug :fetch_session
        plug :protect_from_forgery
        plug :put_secure_browser_headers
      end

      pipeline :when_live do
        plug Legendary.ContentWeb.Plugs.WhenLive
      end

      scope "/", Legendary.Content do
        pipe_through([:browser, :require_auth, :require_admin])

        put "/posts/preview", PostsController, :preview, as: :post_preview
        post "/posts/preview", PostsController, :preview, as: :post_preview
      end

      scope "/", Legendary.Content do
        # Use the default browser stack
        pipe_through :feed

        get "/category/:category/feed.rss", FeedsController, :index, as: :category_feed
        get "/feed.rss", FeedsController, :index, as: :index_feed
      end

      scope "/", Legendary.Content do
        # Use the default browser stack
        pipe_through :browser
        pipe_through :when_live

        live "/", PostsPageLive
        live "/page/:page", PostsPageLive, as: :blog_page
        resources "/sitemap", SitemapController, only: [:index]

        live "/blog", PostsPageLive
        live "/:id", PostPageLive
        live "/category/:category", PostsPageLive, as: :category
        live "/category/:category/page/:page", PostsPageLive, as: :category_page

        live "/*id", PostPageLive, as: :nested_post
      end
    end
  end
end
