defmodule Legendary.Content.FeedsView do
  use Legendary.Content, :html
  use Phoenix.HTML

  import Legendary.Content.LayoutView, only: [excerpt: 2, title: 2]

  def unauthenticated_post?(post) do
    post.password == nil || String.length(post.password) == 0
  end

  Phoenix.Template.compile_all(
    &(&1 |> Path.basename() |> Path.rootname(".rss.eex")),
    Path.join(__DIR__, "feeds"),
    "*"
  )
end
