defmodule Legendary.Content.LayoutView do
  use Legendary.Content, :html
  use Phoenix.Component

  import ShorterMaps

  alias Legendary.ContentWeb.Uploaders.SocialMediaPreview

  def feed_tag(conn, assigns), do: feed_tag(conn, view_module(conn), view_template(conn), assigns)

  def feed_tag(conn, view_module, view_template, outer_assigns) do
    assigns = ~M{conn, view_module, view_template, outer_assigns}

    ~H"""
    <link
      rel="alternate"
      type="application/rss+xml"
      title={title(@view_module, @view_template, @outer_assigns)}
      href={corresponding_feed_url(@conn, @view_module, @view_template, @outer_assigns)}
    />
    """
  end

  def title(conn, assigns), do: title(view_module(conn), view_template(conn), assigns)

  def title(Legendary.Content.PostsView, "index.html", assigns) do
    "Page #{assigns.page} | #{title(nil, nil, nil)}"
  end

  def title(Legendary.Content.FeedsView, "index.rss", %{category: category})
      when not is_nil(category) do
    "#{category} | #{title(nil, nil, nil)}"
  end

  def title(Legendary.Content.PostsView, "show.html", assigns) do
    (assigns.post.title |> HtmlSanitizeEx.strip_tags()) <> " | " <> title(nil, nil, nil)
  end

  def title(_, _, _), do: Legendary.I18n.t!("en", "site.title")

  def excerpt(conn, assigns), do: excerpt(view_module(conn), view_template(conn), assigns)

  def excerpt(Legendary.Content.PostsView, "show.html", assigns) do
    assigns.post.excerpt
    |> HtmlSanitizeEx.strip_tags()
  end

  def excerpt(Legendary.Content.FeedsView, "index.rss", %{category: category})
      when not is_nil(category) do
    "#{category} | #{excerpt(nil, nil, nil)}"
  end

  def excerpt(_, _, _), do: Legendary.I18n.t!("en", "site.excerpt")

  def corresponding_feed_url(conn, _, _, %{category: nil}) do
    Legendary.Content.Router.Helpers.index_feed_url(conn, :index)
  end

  def corresponding_feed_url(conn, Legendary.Content.PostsView, "index.html", %{
        category: category
      }) do
    Legendary.Content.Router.Helpers.category_feed_url(conn, :index, category)
  end

  def corresponding_feed_url(conn, _, _, _) do
    Legendary.Content.Router.Helpers.index_feed_url(conn, :index)
  end

  def modified_tag(conn, assigns),
    do: modified_tag(view_module(conn), view_template(conn), assigns)

  def modified_tag(_, _, %{post: post}) when not is_nil(post) do
    content =
      post.modified_gmt
      |> DateTime.from_naive!("Etc/UTC")
      |> DateTime.to_iso8601()

    tag(:meta, property: "article:modified_time", content: content)
  end

  def modified_tag(_, _, _) do
    nil
  end

  def published_tag(conn, assigns),
    do: modified_tag(view_module(conn), view_template(conn), assigns)

  def published_tag(_, _, %{post: post}) when not is_nil(post) do
    content =
      post.date_gmt
      |> DateTime.from_naive!("Etc/UTC")
      |> DateTime.to_iso8601()

    tag(:meta, property: "article:published_time", content: content)
  end

  def published_tag(_, _, _) do
    nil
  end

  def preview_image_tags(_conn, %{post: post}) when not is_nil(post) do
    url = SocialMediaPreview.url({"original.png", post}, :original)

    [
      tag(:meta, itemprop: "image", content: url),
      tag(:meta, name: "twitter:image:src", content: url),
      tag(:meta, property: "og:image", content: url)
    ]
  end

  def preview_image_tags(_, _) do
    nil
  end

  embed_templates "../components/layouts/*"
end
