defmodule Legendary.Content.SitemapView do
  use Legendary.Content, :html
  use Phoenix.Component

  # sobelow_skip ["XSS.Raw"]
  def raw_content(text) do
    Phoenix.HTML.raw(text)
  end

  embed_templates "sitemap/*"
end
