defmodule Legendary.Content.LiveHelpers do
  @moduledoc """
  Commonly functions for LiveViews.
  """
  use Phoenix.Component

  alias Phoenix.LiveView

  def assign_defaults(socket, session) do
    assign_new(socket, :current_user, fn -> get_user(socket, session) end)
  end

  def require_auth(socket) do
    case socket.assigns do
      %{current_user: user} when not is_nil(user) ->
        socket

      _ ->
        LiveView.redirect(socket, to: "/")
    end
  end

  # Get client IP address
  # @see https://hexdocs.pm/phoenix_live_view/Phoenix.LiveView.html#get_connect_info/1-examples
  def get_ip_address(socket) do
    if info = LiveView.get_connect_info(socket, :peer_data) do
      info.address
    else
      nil
    end
  end

  def author(post) do
    post.author ||
      %Legendary.Auth.User{
        email: "example@example.org",
        display_name: "Anonymous",
        homepage_url: "#"
      }
  end

  def post_element_suffix(%{id: id, sticky: true}), do: "#{id}--sticky"
  def post_element_suffix(%{id: id}), do: id

  defp get_user(socket, session, config \\ [otp_app: :core])

  defp get_user(socket, %{"core_auth" => signed_token}, config) do
    {otp_app, _config} = Keyword.pop(config, :otp_app, :core)
    {store, store_config} = Pow.Plug.Base.store(Application.get_env(otp_app, :pow))

    conn = struct!(Plug.Conn, secret_key_base: socket.endpoint.config(:secret_key_base))
    salt = Atom.to_string(Pow.Plug.Session)

    with {:ok, token} <- Pow.Plug.verify_token(conn, salt, signed_token, config),
         {user, _metadata} <- store.get(store_config, token) do
      user
    else
      _any -> nil
    end
  end

  defp get_user(_, _, _), do: nil
end
