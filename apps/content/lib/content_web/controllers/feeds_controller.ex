defmodule Legendary.Content.FeedsController do
  use Legendary.Content, :controller

  alias Legendary.Content.{Posts}

  @endpoint Legendary.Content.Endpoint
  use Legendary.Content, :verified_routes

  def index(conn, params) do
    category = params |> Map.get("category")
    posts = Posts.list_posts(params)
    thumbs = posts |> Posts.thumbs_for_posts()

    feed_url =
      case category do
        nil ->
          ~p"/feed.rss"

        _ ->
          ~p"/category/#{category}/feed.rss"
      end

    conn
    |> put_resp_content_type("text/xml")
    |> render(
      "index.rss",
      posts: posts,
      thumbs: thumbs,
      category: category,
      feed_url: feed_url
    )
  end
end
