defmodule Legendary.Content.PostsController do
  use Legendary.Content, :controller

  alias Legendary.Content.Posts

  plug :put_layout, false when action in [:preview]

  def preview(conn, %{"post" => post_params}) do
    post = Posts.preview_post(post_params)

    conn
    |> render("show.html", post: post, page: 1, thumbs: [])
  end
end
