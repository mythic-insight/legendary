defmodule Legendary.Content.Router do
  use Legendary.Content, :router
  alias Legendary.AuthWeb.Plugs.{RequireAdmin}

  @host Application.compile_env(:content, :host)
  @content_security_policy (case Mix.env() do
                              :prod ->
                                "default-src 'self';connect-src wss://#{@host};img-src 'self' blob:;font-src 'self' data:"

                              _ ->
                                "default-src 'self' 'unsafe-eval' 'unsafe-inline';" <>
                                  "connect-src ws://#{@host}:*;" <>
                                  "img-src 'self' blob: data:;"

                                "font-src 'self' data:;"
                            end)

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers, %{"content-security-policy" => @content_security_policy}
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :require_admin do
    plug(RequireAdmin)
  end

  pipeline :require_auth do
    plug Pow.Plug.RequireAuthenticated, error_handler: Pow.Phoenix.PlugErrorHandler
  end

  use Legendary.Content.Routes
end
