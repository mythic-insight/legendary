defmodule Legendary.ContentWeb.Plugs.WhenLive do
  @moduledoc """
  A plug that handles some edge cases in the Content engine that can't be handled
  through LiveView:
    - non-HTML (aka "attachment") posts
    - The front page, which sometimes is the blog, or sometimes is a page

  Both of these cases require manipulating the `conn` in ways that are not possible
  at the LiveView layer.
  """
  import Plug.Conn
  import Phoenix.LiveView.Controller
  alias Legendary.Content.Options
  alias Legendary.Content.Posts
  alias Legendary.Content.Posts.Static
  alias Phoenix.Controller

  def init(opts) do
    opts
  end

  def call(conn, _opts) do
    cond do
      conn.request_path == "/" ->
        front_page(conn)

      is_single_post_page?(conn) ->
        try_attachment(conn)

      true ->
        conn
    end
  end

  def front_page(conn) do
    show_on_front = Options.get_value("show_on_front") || "page"

    case show_on_front do
      "posts" ->
        conn

      "page" ->
        page_id = Options.get_value("page_on_front") || "index"
        post = Posts.get_post(page_id)

        render_post(conn, post, page_id)
    end
  end

  defp render_post(conn, nil, slug) do
    conn
    |> Controller.put_view(Static)
    |> Controller.render("#{slug}.html")
    |> halt()
  end

  defp render_post(conn, post, slug) do
    conn
    |> live_render(
      Legendary.Content.PostPageLive,
      router: Legendary.Content.Router,
      session: %{
        "id" => slug,
        "post" => post
      }
    )
    |> halt()
  end

  # sobelow_skip ["XSS.SendResp", "XSS.ContentType"]
  def try_attachment(%{path_params: %{"id" => id}} = conn) do
    post = Posts.get_post(id)

    if post && post.type == "attachment" do
      {:ok, decoded} = post.content |> Base.decode64()

      conn
      |> put_resp_content_type(post.mime_type, charset(post.mime_type))
      |> send_resp(conn.status || 200, decoded)
      |> halt()
    else
      conn
    end
  end

  defp is_single_post_page?(%{private: %{phoenix_live_view: lv_info}}),
    do: elem(lv_info, 0) == Legendary.Content.PostPageLive

  defp is_single_post_page?(_conn), do: false

  defp charset(mime_type) do
    do_charset(String.split(mime_type, "/"))
  end

  defp do_charset(["application", _]), do: "binary"
  defp do_charset(["video", _]), do: "binary"
  defp do_charset(["audio", _]), do: "binary"
  defp do_charset(["image", _]), do: "binary"
  defp do_charset(_), do: "utf-8"
end
