defmodule Legendary.Content do
  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, views, channels and so on.

  This can be used in your application as:

      use Legendary.Content, :controller
      use Legendary.Content, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define any helper function in modules
  and import those modules here.
  """

  def static_paths, do: ~w(css fonts images js favicon.ico robots.txt public_uploads app admin fa)

  def controller do
    quote do
      use Phoenix.Controller, namespace: Legendary.Content

      import Plug.Conn
      import Legendary.Content.Gettext
      alias Legendary.Content.Router.Helpers, as: Routes
    end
  end

  def live_view do
    quote do
      use Phoenix.LiveView,
        layout: {Legendary.Content.Layouts, :app}

      import Legendary.Content.LiveHelpers

      unquote(html_helpers())
    end
  end

  def live_component do
    quote do
      use Phoenix.LiveComponent

      import Legendary.Content.LiveHelpers

      unquote(html_helpers())
      unquote(verified_routes())
    end
  end

  def html do
    quote do
      use Phoenix.Component

      # Import convenience functions from controllers
      import Phoenix.Controller,
        only: [get_csrf_token: 0, get_flash: 1, get_flash: 2, view_module: 1, view_template: 1]

      # Include general helpers for rendering HTML
      unquote(html_helpers())
    end
  end

  def verified_routes do
    quote do
      use Phoenix.VerifiedRoutes,
        endpoint: Legendary.Content.Endpoint,
        router: Legendary.Content.Router,
        statics: Legendary.Content.static_paths()
    end
  end

  def router do
    quote do
      use Phoenix.Router

      import Plug.Conn
      import Phoenix.Controller
      import Phoenix.LiveView.Router
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import Legendary.Content.Gettext
    end
  end

  defp html_helpers do
    quote do
      # Use all HTML functionality (forms, tags, etc)
      import Phoenix.HTML
      import Phoenix.HTML.Tag
      # Core UI components
      import Legendary.Content.CoreComponents
      import Phoenix.LiveView.Helpers

      # Shortcut for generating JS commands
      alias Phoenix.LiveView.JS

      def gravatar_url_for_email(email) do
        email
        |> Kernel.||("noreply@example.com")
        |> String.trim()
        |> String.downcase()
        |> (&:crypto.hash(:md5, &1)).()
        |> Base.encode16()
        |> String.downcase()
        |> (&"https://www.gravatar.com/avatar/#{&1}").()
      end

      def process_content(text) do
        text
        |> Earmark.as_html!()
      end

      import Legendary.Content.ErrorHelpers
      import Legendary.Content.Gettext
      import Legendary.CoreWeb.Helpers
      import Phoenix.LiveView.Helpers
      alias Legendary.Content.Router.Helpers, as: Routes
      unquote(verified_routes())
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
