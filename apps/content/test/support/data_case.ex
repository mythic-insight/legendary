defmodule Legendary.Content.DataCase do
  @moduledoc """
  This module defines the setup for tests requiring
  access to the application's data layer.

  You may define functions here to be used as helpers in
  your tests.

  Finally, if the test case interacts with the database,
  it cannot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """

  use ExUnit.CaseTemplate

  import Phoenix.Component, only: [sigil_H: 2]
  import Phoenix.HTML.Form, only: [form_for: 4]

  alias Ecto.Adapters.SQL.Sandbox
  alias Ecto.Changeset
  alias Phoenix.HTML.Safe

  using do
    quote do
      alias Legendary.Content.Repo

      import Ecto

      import Ecto.Query
      import Legendary.Content.DataCase
    end
  end

  setup tags do
    :ok = Sandbox.checkout(Legendary.Content.Repo)

    unless tags[:async] do
      Sandbox.mode(Legendary.Content.Repo, {:shared, self()})
    end

    :ok
  end

  @doc """
  A helper that transform changeset errors to a map of messages.

      assert {:error, changeset} = Accounts.create_user(%{password: "short"})
      assert "password is too short" in errors_on(changeset).password
      assert %{password: ["password is too short"]} = errors_on(changeset)

  """
  def errors_on(changeset) do
    Changeset.traverse_errors(changeset, fn {message, opts} ->
      Enum.reduce(opts, message, fn {key, value}, acc ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
  end

  def stringify(safe) do
    safe
    |> Safe.to_iodata()
    |> IO.iodata_to_binary()
  end

  def with_form(func) do
    assigns = %{func: func}

    ~H"""
    <%= form_for :example,
      "/example",
      [
        as: :test_params,
        errors: [error_field: {"is an error", []}]
      ],
      fn f -> %>
      <%= @func.(f) %>
    <% end %>
    """
  end
end
