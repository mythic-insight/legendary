defmodule Legendary.Content.MarkupFieldTest do
  use Legendary.Content.DataCase

  import Legendary.Content.MarkupField

  test "underlying db type is string" do
    assert type() == :string
  end

  test "cast string to markup field" do
    assert cast("foo") == {:ok, "foo"}
  end

  test "cast nonstring to markup field is an error" do
    assert cast(1) == :error
  end

  test "dump string from markup field" do
    assert dump("baz") == {:ok, "baz"}
  end

  test "dump nonstring from markup field is error" do
    assert dump(3) == :error
  end

  test "load value" do
    assert load("bar") == {:ok, "bar"}
  end

  test "render_form/5 makes a field with a simplemde data attribute" do
    markup =
      with_form(fn f ->
        render_form(nil, nil, f, :boop, %{})
      end)
      |> stringify()

    assert markup =~ "data-simplemde"
  end

  test "render_index/4 can render markdown" do
    markup = render_index(nil, %{text: "# Test"}, :text, [])

    assert markup =~ "<h1>"
    assert markup =~ "Test"
  end
end
