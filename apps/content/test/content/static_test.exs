defmodule Legendary.Content.Posts.StaticTest do
  use Legendary.Content.DataCase

  import Legendary.Content.Posts.Static

  describe "static/1" do
    test "a non-existant static page returns nil", do: assert(static("zzzzz") == nil)

    test "handles a static page" do
      assert static("index")
    end
  end
end
