defmodule Legendary.Content.PostComponentTest do
  use Legendary.Content.ConnCase, async: true
  use Phoenix.Component

  import Phoenix.LiveViewTest

  alias Legendary.Auth.User
  alias Legendary.Content.Post
  alias Legendary.Content.PostComponent

  test "post_pagination/1 with current_page" do
    content = """
    Uno
    <!--nextpage-->
    Dos
    <!--nextpage-->
    Tres
    """

    post = %Post{name: "test", content: content}

    assigns = %{post: post}

    markup =
      ~H"""
      <PostComponent.post_pagination post={@post} current_page={2} />
      """
      |> stringify()

    assert markup =~ ~s|<span class="paginator-page">2</span>|
    assert markup =~ ~s|href="/posts/test?page=1"|
    assert markup =~ ~s|href="/posts/test?page=3"|
  end

  test "post_pagination/1" do
    content = """
    Uno
    <!--nextpage-->
    Dos
    <!--nextpage-->
    Tres
    """

    post = %Post{name: "test", content: content}

    assigns = %{post: post}

    markup =
      ~H"""
      <PostComponent.post_pagination post={@post} />
      """
      |> stringify()

    refute markup =~ ~s|<span class="paginator-page">2</span>|
    assert markup =~ ~s|<span class="paginator-page">1</span>|
    assert markup =~ ~s|href="/posts/test?page=3"|
  end

  describe "password-protected posts" do
    def post_assigns(extra \\ %{}) do
      post = %Post{
        name: "pass",
        content: "Secret Post Content",
        password: "cadabra",
        categories: [],
        author: %User{
          homepage_url: "https://example.com",
          display_name: "Example Author"
        },
        date: ~N"2023-03-15T00:00:00"
      }

      Map.merge(%{id: "foo", post: post, thumbs: [], single: true, page: 1}, extra)
    end

    test "without password shows password form" do
      assigns = post_assigns()
      assert render_component(PostComponent, assigns) =~ "Password Protected Post"
      refute render_component(PostComponent, assigns) =~ "Secret Post Content"
    end

    test "with incorrect password shows the password form" do
      assigns = post_assigns(%{password: "abra"})
      assert render_component(PostComponent, assigns) =~ "Password Protected Post"
      refute render_component(PostComponent, assigns) =~ "Secret Post Content"
    end

    test "with correct password shows the post" do
      assigns = post_assigns(%{password: "cadabra"})
      assert render_component(PostComponent, assigns) =~ "Secret Post Content"
    end
  end

  describe "render_authenticated/1" do
    def post_assigns_content(extras \\ %{}) do
      {content_extras, extras} = Map.pop(extras, :post, %{})

      post =
        %Post{
          name: "pass",
          content: "Basic content",
          categories: [],
          author: %User{
            homepage_url: "https://example.com",
            display_name: "Example Author"
          },
          date: ~N"2023-03-15T00:00:00",
          comment_status: "open",
          comments: []
        }
        |> Map.merge(content_extras)

      Map.merge(
        %{id: "foo", post: post, thumbs: [], single: true, page: 1, ip_address: "127.0.0.1"},
        extras
      )
    end

    test "post with more below the fold" do
      assigns = post_assigns_content(%{post: %{content: "ATF\n<!--more-->\nBTF"}, single: false})
      markup = render_component(PostComponent, assigns)

      assert markup =~ "ATF"
      assert markup =~ "Keep Reading"
      refute markup =~ "BTF"

      assigns = post_assigns_content(%{post: %{content: "ATF\n<!--more-->\nBTF"}})
      markup = render_component(PostComponent, assigns)

      assert markup =~ "ATF"
      refute markup =~ "Keep Reading"
      assert markup =~ "BTF"
    end

    test "post without categories" do
      assigns = post_assigns_content()
      markup = render_component(PostComponent, assigns)

      refute markup =~ "Categories:"
    end

    test "post with categories" do
      assigns = post_assigns_content(%{post: %{categories: [%{name: "test", slug: "test-slug"}]}})
      markup = render_component(PostComponent, assigns)

      assert markup =~ "Categories"
      assert markup =~ "test"
      assert markup =~ "/category/test-slug"
    end

    test "post with comments on" do
      assigns = post_assigns_content()
      markup = render_component(PostComponent, assigns)

      assert markup =~ "Comments"
    end

    test "post with comments off" do
      assigns = post_assigns_content(%{post: %{comment_status: "closed"}})
      markup = render_component(PostComponent, assigns)

      refute markup =~ "Comments"
    end
  end
end
