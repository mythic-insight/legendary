defmodule Legendary.Content.LayoutsTest do
  use Legendary.Content.ConnCase, async: true

  import Legendary.Content.Layouts

  describe "title/3" do
    def default_title do
      Legendary.I18n.t!("en", "site.title")
    end

    test "for category" do
      assert title(Legendary.Content.PostsView, "show.html", %{post: %{title: "Test"}}) =~
               "Test | #{default_title()}"
    end
  end

  describe "excerpt/3" do
    test "for a post" do
      assert excerpt(Legendary.Content.PostsView, "show.html", %{post: %{excerpt: "Excerpt test"}}) =~
               "Excerpt test"
    end
  end
end
