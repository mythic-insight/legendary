defmodule Legendary.Content.ErrorHelpersTest do
  use Legendary.Content.DataCase

  import Legendary.Content.ErrorHelpers

  describe "error_tag/2" do
    test "generates a span with an invalid-feedback class" do
      safe =
        with_form(fn f ->
          error_tag(f, :error_field)
        end)
        |> stringify()

      assert safe =~ "invalid-feedback"
    end

    test "error_tag/3" do
      safe =
        with_form(fn f ->
          error_tag(f, :error_field, class: "test-class")
        end)
        |> stringify()

      assert safe =~ "test-class"
    end
  end
end
