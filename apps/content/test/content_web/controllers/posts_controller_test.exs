defmodule Legendary.Content.PostsControllerTest do
  use Legendary.Content.ConnCase

  @preview_attrs %{
    name: "my-post",
    title: "My Post",
    content: "",
    status: "publish",
    type: "post",
    date: "2018-01-01T00:00:00Z"
  }

  describe "preview a post" do
    test "shows the post (post method)", %{conn: conn} do
      conn =
        as_admin do
          post(conn, Routes.post_preview_path(conn, :preview, %{"post" => @preview_attrs}))
        end

      assert html_response(conn, 200) =~ @preview_attrs[:title]
    end

    test "shows the post (put method)", %{conn: conn} do
      conn =
        as_admin do
          put(conn, Routes.post_preview_path(conn, :preview, %{"post" => @preview_attrs}))
        end

      assert html_response(conn, 200) =~ @preview_attrs[:title]
    end
  end
end
