defmodule Legendary.Content.ReplyFormComponentTest do
  use Legendary.Content.ConnCase

  import Phoenix.LiveViewTest

  alias Legendary.Content.Endpoint
  alias Legendary.Content.Posts
  import Legendary.Content.Router.Helpers

  @post_attrs %{id: 456, name: "blergh", status: "publish", type: "post", comment_status: "open"}

  def fixture(:post) do
    {:ok, post} = Posts.create_posts(@post_attrs)
    post
  end

  describe "create comment" do
    test "redirects to show when data is valid", %{conn: conn} do
      post = fixture(:post)
      {:ok, view, _html} = live(conn, live_path(Endpoint, Legendary.Content.PostPageLive, post))

      view
      |> element("form")
      |> render_submit(%{
        comment: %{
          author: "Test Author",
          author_email: "test@example.org",
          author_url: "http://example.org",
          content: "My insightful comment!"
        }
      })

      assert_redirected(view, Routes.live_path(conn, Legendary.Content.PostPageLive, post))
    end

    test "renders errors when data is invalid", %{conn: conn} do
      post = fixture(:post)
      {:ok, view, _html} = live(conn, live_path(Endpoint, Legendary.Content.PostPageLive, post))

      view
      |> element("form")
      |> render_submit(%{comment: %{}})

      markup = render(view)

      assert markup =~ "something went wrong"
    end
  end
end
