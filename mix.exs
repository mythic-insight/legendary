defmodule Legendary.Mixfile do
  use Mix.Project

  @version "8.12.0"

  def project do
    [
      name: "Legendary",
      version: @version,
      apps_path: "apps",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      aliases: aliases(),
      dialyzer: [
        plt_add_apps: [:mix],
        ignore_warnings: ".dialyzer_ignore.exs"
      ],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test,
        "coveralls.json": :test,
        "coveralls.xml": :test,
        "coveralls.lcov": :test
      ],
      releases: [
        app: [
          applications: [
            admin: :permanent,
            app: :permanent,
            content: :permanent,
            core: :permanent,
            object_storage: :permanent
          ],
          include_executables_for: [:unix],
          cookie: "GFxlbUD2KkIL1mGpPxvuAse26YZusbw-7Px-gZJsYw66xRVMRuH5TA==",
          steps: [:assemble, &copy_assets/1]
        ]
      ]
    ]
  end

  def copy_assets(%{path: release_root} = release) do
    static_path = Path.expand("apps/app/priv/static", __DIR__)
    target_path = Path.join([release_root, "apps", "app", "priv", "static"])
    File.mkdir_p!(target_path)
    File.cp_r!(static_path, target_path)

    release
  end

  defp deps do
    []
  end

  defp aliases do
    [
      "deps.get": ["cmd mix deps.get"],
      "ecto.migrate": ["cmd mix ecto.migrate"],
      "npm.install": ["cmd mix npm.install"],
      sobelow: ["cmd mix sobelow --config --skip"]
    ]
  end
end
