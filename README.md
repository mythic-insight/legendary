# ARCHIVE NOTICE

Hello there.

We've decided to archive this repo and discontinue work on Legendary.

We started work on Legendary with a much earlier version of Phoenix and Elixir, before LiveView was even announced. The PETAL stack seemed like The Way for building Phoenix apps. A lot has changed that has invalidated our core assumptions and value for this project!

- Phoenix LiveView has mostly eliminated the need for even JS sprinkles beyond the occasional hook. Most projects simply do not need the big asset pipeline that we have for Legendary.
- The community has more or less standardized on phx.gen.auth over pow.
- The templates that come out of the box with Phoenix now are better than the ones we added to Legendary.
- My grudging respect for Docker & Kubernetes has slowly morphed into bitter acrimony. Now when people ask the best way to deploy Phoenix on k8, I don't say "we have a template for that", I say "You really don't want to do that."

I think we were on a very reasonable course at the time the project started, but things change. We'd rather work on things that move the community more in the direction that it wants to go!

I think there are some bits of the project that ARE still good ideas, and they might re-emerge as separate packages:

- There's still a need for a very good content management system on top of Phoenix. Various smart people are working on that, and I think we could contribute something there.
- I think the object-storage engine is a still a great idea since it makes development easier and potentially eliminates S3 as a dependency for many apps.

And we have lots of new ideas too! That's part of why we're archiving the project-- to have the bandwidth to help out the community more productively.

# Overview

Legendary is a boilerplate for developing [PETAL-stack](https://changelog.com/posts/petal-the-end-to-end-web-stack)
Phoenix/Elixir applications without reinventing the wheel. Out-of-the-box, we
include many features that are commonly needed in web applications:

- Features
  - Authentication & Authorization
  - Admin interface & dashboard
  - Lightweight content management / blogging
  - Background & scheduled jobs with Oban
- Frontend Frameworks
  - Tailwind CSS
  - Alpine JS
  - Fluid HTML email templates
- Full CI / DevOps scripts included

We got tired of setting these things up from scratch on every Phoenix application.
So, we built a boilerplate that lets you start with the unique & interesting thing
that only your application does. We have a roadmap for future feature development
because we still think there are a lot more things we can do to make Phoenix
development better.

## Up and Running

In order to start the server, run `script/server`. Any dependencies required
will be installed automatically using [brew](https://brew.sh/),
[asdf](https://asdf-vm.com/#/), and [hex](https://hex.pm/).

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Application Development

Your main app lives in apps/app/ and that is where you will do most of your
development there. This is a normal Phoenix application and you can develop it
as such. Any resources which apply to developing Phoenix applications will apply
inside of the app. See the [Phoenix Guides](https://hexdocs.pm/phoenix/overview.html)
for a good starting resource in Phoenix development.

You should not generally need to change code in the other applications which
are part of the framework-- admin, content, core. We encourage you to avoid
changing those as much as possible, because doing so will make it more difficult
to upgrade Legendary to newer versions. However, they are available to you if
you find that there are no other ways to accomplish the changes you want to
accomplish. If you find yourself adding functionality to admin, content, or core
that you feel would be beneficial to all Legendary apps, consider making a
code contribution back to the framework!

## CI Configuration

Legendary comes with gitlab CI settings which should work for you with minimal
setup.

The CI script will automatically tag successful builds. To do this, you will
need to configure a [CI variable](-/settings/ci_cd) named `GITLAB_TOKEN`. This
token should be a [personal access token](/-/profile/personal_access_tokens) with
`read_repository, write_repository` permissions.


# Support

Want to support development? Chip in on buymeacoffee:

<a href="https://www.buymeacoffee.com/prehnra" target="_blank">
  <img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" alt="Buy Me A Coffee" style="height: 60px !important;width: 217px !important;" >
</a>
