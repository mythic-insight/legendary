FROM elixir:1.14.4-alpine AS elixir-builder

RUN apk add git

RUN mix local.hex --force \
  && mix local.rebar --force

EXPOSE 4000
# Default EPMD port
EXPOSE 4369

ARG MIX_ENV=prod
ARG ECTO_IPV6=false
ARG ERL_AFLAGS=""

ENV PORT=4000
ENV LANG C.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

ENV MIX_ENV=${MIX_ENV}
ENV HOSTNAME=${HOSTNAME}
ENV FLY_APP_NAME=${FLY_APP_NAME}
ENV ECTO_IPV6=${ECTO_IPV6}
ENV ERL_AFLAGS=${ERL_AFLAGS}

WORKDIR /root/app

# We load these things one by one so that we can load the deps first and
#   cache those layers, before we do the app build itself
ADD ./priv ./priv
ADD ./mix.exs ./mix.lock ./
ADD config/config.exs config/${MIX_ENV}.exs config/admin.exs config/email_styles.exs config/runtime.exs config/
ADD ./apps/admin/mix.exs ./apps/admin/
ADD ./apps/app/mix.exs ./apps/app/
ADD ./apps/content/mix.exs ./apps/content/
ADD ./apps/core/mix.exs ./apps/core/
ADD ./apps/object_storage/mix.exs ./apps/object_storage/

RUN mix deps.get --only $MIX_ENV
RUN mix deps.compile

# Leave off here so that we can built assets and compile the elixir app in parallel

FROM node:20.1.0 AS asset-builder

# Build assets in a node container
ADD ./apps/app/assets /root/app/apps/app/assets
# Needed to properly purge CSS-- directories which may contain view code
ADD ./apps/admin/lib /root/app/apps/admins/lib
ADD ./apps/app/lib /root/app/apps/app/lib
ADD ./apps/content/lib /root/app/apps/content/lib
ADD ./apps/core/lib /root/app/apps/core/lib
ADD ./apps/object_storage/lib /root/app/apps/object_storage/lib

WORKDIR /root/app/apps/app/assets/
COPY --from=0 /root/app/deps/ /root/app/deps/
RUN npm install

RUN npm run deploy

FROM elixir-builder as elixir-releaser

# Copy in the built assets & fingerprint them
COPY --from=asset-builder /root/app/apps/app/priv/static/ /root/app/apps/app/priv/static
RUN mix phx.digest

ADD ./apps/admin/lib /root/app/apps/admin/lib
ADD ./apps/app/lib /root/app/apps/app/lib
ADD ./apps/content/lib /root/app/apps/content/lib
ADD ./apps/core/lib /root/app/apps/core/lib
ADD ./apps/object_storage/lib /root/app/apps/object_storage/lib
ADD ./apps/admin/priv /root/app/apps/admin/priv
ADD ./apps/app/priv /root/app/apps/app/priv
ADD ./apps/content/priv /root/app/apps/content/priv
ADD ./apps/core/priv /root/app/apps/core/priv
ADD ./apps/object_storage/priv /root/app/apps/object_storage/priv
COPY config/i18n config/i18n

COPY rel rel
RUN mix release

FROM alpine as runner

ENV LANG C.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN apk update && apk add openssl ncurses-libs libgcc libstdc++

WORKDIR "/app"

# Copy the final release
COPY --from=elixir-releaser --chown=nobody:root /root/app/_build/prod/rel/app ./
RUN mkdir -p /app/bin/priv
RUN chown -R nobody: /app
USER nobody

CMD /app/bin/server
